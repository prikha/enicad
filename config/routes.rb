Rails.application.routes.draw do
  root to: 'accounts#show'

  devise_for :users
end
