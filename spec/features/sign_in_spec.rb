require 'rails_helper'

feature 'User signs in' do
  let(:user) { create :user, email: 'user@mail.ru', password: 'password' }

  scenario 'with valid credentials' do
    visit root_path

    fill_in 'Электронная почта', with: user.email
    fill_in 'Пароль', with: user.password

    click_button 'Войти'

    expect(page).to have_content('Выход')
  end

  scenario 'with invalid credentials' do
    visit root_path

    fill_in 'Электронная почта', with: "incorrect@mail.ru"
    fill_in 'Пароль', with: 'wrongpassword'
    click_button 'Войти'

    expect(page).to have_content('Неверный адрес эл. почты или пароль.')
  end
end