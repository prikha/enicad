require 'rails_helper'

feature 'User signs up' do
  scenario 'via email and password' do
    visit root_path

    click_link 'Регистрация'

    fill_in 'Электронная почта', with: 'some@mail.ru'
    fill_in 'Пароль', with: '123456'
    fill_in 'Подрверждение пароля', with: '123456'

    click_button 'Зарегистрироваться'

    expect(page).to have_link('Выход')
  end
end