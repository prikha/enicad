require 'rails_helper'

feature 'User signs out' do
  let(:user) { create :user, email: 'user@mail.ru', password: 'password' }

  scenario 'from account' do
    visit root_path

    fill_in 'Электронная почта', with: user.email
    fill_in 'Пароль', with: user.password

    click_button 'Войти'

    expect(page).to have_link('Выход')

    click_link 'Выход'

    expect(page).to have_button('Войти')
  end
end

