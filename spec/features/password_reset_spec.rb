require 'rails_helper'

feature 'User resets a password' do
  background { clear_emails }

  scenario 'via email' do
    create :user, email: 'some@mail.ru'

    visit root_path

    click_link 'Забыли пароль?'

    fill_in 'Электронная почта', with: 'some@mail.ru'

    click_button 'Отправить'

    open_email('some@mail.ru')

    current_email.click_link "ссылка для смены пароля"

    expect(page).to have_button("Сменить пароль")
  end
end