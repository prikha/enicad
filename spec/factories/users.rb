FactoryGirl.define do
  factory :user do
    email { "user#{rand}@mail.ru" }
    password { SecureRandom.hex(10) }
  end
end